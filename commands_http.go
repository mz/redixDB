// Copyright 2018 The Redix Authors. All rights reserved.
// Use of this source code is governed by a Apache 2.0
// license that can be found in the LICENSE file.
package main

import (
	"crypto/tls"
	"github.com/alash3al/go-color"
	"gopkg.in/resty.v1"
	"log"
	"strconv"
	"strings"
)

// httpGetCommand - HTTPGET <key>:<expire> <url> <headers>
func httpGetCommand(c Context) {
	if len(c.args) < 2 {
		c.WriteError("HTTPGET command must have at least one argument: HTTPGET <key> <url> <headers> <expire>")
		return
	}
	// format key:expire
	keyExpired := strings.SplitN(c.args[0], ":", 2)

	k := keyExpired[0]

	// get in db first
	data, err := c.db.Get(k)
	if err == nil {
		if data != "" {
			c.WriteBulkString(string(data))
			return
		}
	}

	// ttl
	ttlVal := 0
	if len(keyExpired) > 1 {
		ttlVal, err := strconv.Atoi(keyExpired[1])
		if err != nil {
			ttlVal = 0
		}
		if ttlVal < 0 {
			ttlVal = 0
		}
		if *flagVerbose {
			log.Println(color.YellowString("HTTPGET[Expired]"), color.CyanString(strconv.Itoa(ttlVal)))
		}

	}

	// get url
	url := c.args[1]

	// ssl verify false
	resty.SetTLSClientConfig(&tls.Config{InsecureSkipVerify: true})
	// new instance
	R := resty.R()

	// set Header
	for _, header := range c.args[2:] {
		headerKv := strings.SplitN(header, ":", 2)
		R.SetHeader(headerKv[0], headerKv[1])
		if *flagVerbose {
			log.Println(color.YellowString("HTTPGET[SetHeader]"), color.CyanString(strings.Join(headerKv, ": ")))
		}
	}
	// get
	//SetResult(&AuthSuccess{}). // or SetResult(AuthSuccess{}).
	resp, _ := R.Get(url)
	// get response
	v := string(resp.String())

	// set db
	if err := c.db.Set(k, v, ttlVal); err != nil {
		c.WriteError(err.Error())
		return
	}
	if *flagVerbose {
		log.Println(color.YellowString("HTTPGET[Response]"), color.CyanString(v))
	}
	c.WriteBulkString(v)
}
